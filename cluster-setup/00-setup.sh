helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm repo add jetstack https://charts.jetstack.io

# Reference: https://github.com/bitnami-labs/sealed-secrets
helm repo add sealed-secrets https://bitnami-labs.github.io/sealed-secrets
helm repo update

# Install ingress-nginx
helm install ingress-nginx -f values-ingress-nginx.yaml ingress-nginx/ingress-nginx

# Install cert-manager
kubectl apply -f https://github.com/cert-manager/cert-manager/releases/download/v1.10.1/cert-manager.crds.yaml
helm install cert-manager --namespace cert-manager --version v1.10.1 jetstack/cert-manager

# Install Certificate Issuers
kubectl apply -f staging_issuer.yaml
kubectl apply -f prod_issuer.yaml

# Install SealedSecrets
helm install sealed-secrets -n kube-system --set-string fullnameOverride=sealed-secrets-controller sealed-secrets/sealed-secrets

# Install ArgoCD
kubectl create namespace argocd
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml

# Install Argo Server ingress
kubectl apply -f argocd-ingress.yaml

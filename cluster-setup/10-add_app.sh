# Login to ArgoCD
argocd login \
    $(kubectl get service/argocd-server -o jsonpath='{.spec.clusterIP}') \
    --insecure \
    --username "admin" \
    --password $(
        kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d
        echo
    )

# Add mastodon application
# Create secret
kubectl -n default create secret generic mastodon-postgres-secret --from-literal=password=cubicle-pennant-sedate-subsoil-tubby  \
--from-literal=postgres-password=outsource-headrest-baggage \
--dry-run=client -o yaml | kubeseal -o yaml >mastodon-postgres-sealed-secret.yaml



kubectl -n default create secret generic mastodon-server-secret \
--from-literal=login=hzmmohamed@gmail.com \
--from-literal=password=gP2CJvuZxL2h \
--from-literal=redis-password=grapple-spender-obsolete \
--from-literal=OTP_SECRET=6a2454b24acfa12dd9962f6dc268a0a93cc29b8c79ebfd7ee842dac143a8421b219de19b10504eecaa5843e7eb5684e1cb335a292a748761d2fd6c58b4dde93b \
--from-literal=SECRET_KEY_BASE=f91988f85997a9e0cde8c6ca45dbed4685d3b99767339c80f530bf73f05b04b0ce21b252740731199ca9e319180226db11c31fa428ddc1d84d11b2a3545dac3b \
--from-literal=VAPID_PRIVATE_KEY=4uxcqrPGLNxAYsSkgUucEoeMJe9V9XMI8z2yFj7zSgk= \
--from-literal=VAPID_PUBLIC_KEY=BKnta9ZvpcH6F2CeIMB8sKwdM7YRHHvSAQTWfzlCGCq_7hZ2Xn3zv40SkvJcuczTFh3M2sN_Ooh5Cs5WmY7pXjw= \
--dry-run=client \
-o yaml | kubeseal -o yaml > mastodon-server-sealed-secret.yaml
